import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpRequest } from '@angular/common/http';
import 'rxjs/Rx';
import { forkJoin } from 'rxjs';
import { Observable } from 'rxjs/Observable';

import { RecipeService } from '../recipes/recipe.service';
import { Recipe } from '../recipes/recipe.model';
// import { AuthService } from '../auth/auth.service';
// ,
//               private authService: AuthService
@Injectable()
export class DataStorageService {
  constructor(private httpClient: HttpClient,
              private recipeService: RecipeService) {
  }

  readonly url = "https://expenses-557e7.firebaseio.com/";

   storeRecipes2(): Observable<any[]> {
    const req1 = new HttpRequest('PUT', 'https://expenses-557e7.firebaseio.com/recipes.json', this.recipeService.getRecipes(), {reportProgress: true});
    const req2 = new HttpRequest('PUT', 'https://expenses-557e7.firebaseio.com/incomes.json', this.recipeService.getIncomes(), {reportProgress: true}); 
    console.log("REQ"+ req1);
    // Observable.forkJoin (RxJS 5) changes to just forkJoin() in RxJS 6
    return  forkJoin([req1, req2]);
  }

  storeRecipes() {
    // const headers = new HttpHeaders().set('Authorization', 'Bearer afdklasflaldf');

    // return this.httpClient.put('https://ng-recipe-book-3adbb.firebaseio.com/recipes.json', this.recipeService.getRecipes(), {
    //   observe: 'body',
    //   params: new HttpParams().set('auth', token)
    //   // headers: headers
    // });
    const req = new HttpRequest('PUT', 'https://expenses-557e7.firebaseio.com/recipes1.json', this.recipeService.getRecipes(), {reportProgress: true});
    return this.httpClient.request(req);
  } 
  storeIncomes() {
    const req = new HttpRequest('PUT', 'https://expenses-557e7.firebaseio.com/incomes1.json', this.recipeService.getIncomes(), {reportProgress: true});
    return this.httpClient.request(req);
  } 

  getRecipes() {
    // this.httpClient.get<Recipe[]>('https://ng-recipe-book-3adbb.firebaseio.com/recipes.json?auth=' + token)
    this.httpClient.get<Recipe[]>('https://expenses-557e7.firebaseio.com/recipes1.json', {
      observe: 'body',
      responseType: 'json'   
    })
      .map(
        (recipes) => {
          console.log("MAP GETRECIPES" +recipes);
          for (let recipe of recipes) {
            if (!recipe['ingredients']) {
              recipe['ingredients'] = [];
            }
          }
          return recipes;
        }
      )
      .subscribe(
        (recipes: Recipe[]) => {
          this.recipeService.setRecipes(recipes);
        }
      );
  }

  getIncomes() {
    // this.httpClient.get<Recipe[]>('https://ng-recipe-book-3adbb.firebaseio.com/recipes.json?auth=' + token)
    this.httpClient.get<Recipe[]>('https://expenses-557e7.firebaseio.com/incomes1.json', {
      observe: 'body',
      responseType: 'json'   
    })
      .map(
        (recipes) => {
          console.log("Map "+recipes);
          for (let recipe of recipes) {
            if (!recipe['ingredients']) {
              recipe['ingredients'] = [];
            }
          }
          return recipes;
        }
      )
      .subscribe(
        (recipes: Recipe[]) => {
          this.recipeService.setIncomes(recipes);
          this.recipeService.setcurrntrecipes(recipes);
        }
      );
  }
}
