import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';

import { Recipe } from './recipe.model';
import { Ingredient } from '../shared/ingredient.model';
import { ShoppingListService } from '../shopping-list/shopping-list.service';


@Injectable()
export class RecipeService {
  recipesChanged2 = new Subject<Recipe[]>();
  recipesChanged = new Subject<Recipe[]>();
  //Recipe[]
  TESTCHANGE = new Subject<any>();

  private recipes: Recipe[] = [];
  private income: Recipe[] = [];
  private currntrecipes: Recipe[] = [];
  private currntincome: Recipe[] = [];

  constructor(private slService: ShoppingListService) { }

  thisMonth(month) {
    console.log("TEST THIS MONTH" + month);
    let currentYear = new Date().getFullYear();
    this.currntrecipes = this.recipes.filter(function (recipe) {
      return (new Date(recipe.date).getMonth() + 1) == month && (new Date(recipe.date).getFullYear() === currentYear);
    });
    this.currntincome = this.income.filter(function (recipe) {
      return (new Date(recipe.date).getMonth() + 1) == month && (new Date(recipe.date).getFullYear() === currentYear);
    });
    this.setcurrntrecipes(this.currntrecipes);
    const totalEx = this.currntrecipes.reduce((acc, out) => acc + out.money, 0);
    const totalIn = this.currntincome.reduce((acc, out) => acc + out.money, 0);
    console.log("Sum Ex" + totalEx + "Sum In" + totalIn);
    console.log("currntrecipes" + JSON.stringify(this.currntrecipes));
    console.log("currntincome" + JSON.stringify(this.currntincome));
    //this.recipesChanged.next(this.currntrecipes.slice());
    this.recipesChanged2.next(this.currntincome.slice());
    this.TESTCHANGE.next(month);
  }

  getMessage(): Observable<Recipe[]> {
    return this.recipesChanged2.asObservable();
  }

  setRecipes(recipes: Recipe[]) {
    this.recipes = recipes;
    this.recipesChanged.next(this.recipes.slice());
  }
  setcurrntrecipes(recipes: Recipe[]) {
    this.currntrecipes = recipes;
    console.log("IN==setcurrntrecipes" + JSON.stringify(this.currntrecipes));
    this.recipesChanged2.next(this.currntrecipes.slice());
  }

  setIncomes(recipes: Recipe[]) {
    this.income = recipes;
    this.recipesChanged.next(this.income.slice());
  }

  getRecipes() {
    return this.recipes.slice();
  }
  getIncomes() {
    return this.income.slice();
  }

  getRecipes2() {
    return this.currntrecipes.slice();
  }
  getIncomes2() {
    return this.currntincome.slice();
  }

  getRecipe(index: number) {
    return this.recipes[index];
  }
  getIncome(index: number) {
    return this.income[index];
  }
  addIngredientsToShoppingList(ingredients: Ingredient[]) {
    this.slService.addIngredients(ingredients);
  }

  addRecipe(recipe: Recipe) {
    console.log("addrecipe" + JSON.stringify(recipe));
    if (recipe.type == "הוצאה") {
      console.log("inADD");
      this.recipes.push(recipe);
      this.recipesChanged.next(this.recipes.slice());
      this.recipesChanged2.next(this.recipes.slice());
    }
    else if (recipe.type == "הכנסה") {
      {
        this.income.push(recipe);
        this.recipesChanged.next(this.income.slice());
      }
    }
  }

  updateRecipe(index: number, newRecipe: Recipe) {
    console.log("newRecipe" + JSON.stringify(newRecipe))
    if (newRecipe.type == "הוצאה") {
      console.log("if (1newRecipe.type ==  ");
      this.recipes[index] = newRecipe;
      this.recipesChanged.next(this.recipes.slice());
      this.recipesChanged2.next(this.recipes.slice());
      console.log("OUT OUT 1 ");
    }
    else if (newRecipe.type == "הכנסה") {
      console.log("if (2newRecipe.type ==  ");
      this.income[index] = newRecipe;
      this.recipesChanged.next(this.income.slice());
      console.log("OUT OUT 2 ");
    }
  }


  deleteRecipe(index: number, type: string) {
    if (type == "הוצאה") {
      this.recipes.splice(index, 1);
      this.recipesChanged.next(this.recipes.slice());
    }
    else {
      this.income.splice(index, 1);
      this.recipesChanged.next(this.income.slice());
    }
  }

  setPicCat(name: string) {
    var url = "";
    switch (name) {
      case "בילוי ומסעדות":
        url = "http://www.romabest.co.il/wp-content/uploads/2013/01/assunta_madre1.jpg";
        console.log("IN SWITCH");
        break;
      case "מזון ומשקאות":
        url = "http://yochananof.co.il/wp-content/uploads/2016/07/logo.png";
        break;
      case "בריאות":
        url = "How you like them apples?";
        break;
      case "ביגוד והנעלה":
        url = "How you like them apples?";
        break;
      case "מוצרי חשמל ותקשורת":
        url = "How you like them apples?";
        break;
      case "רכב ותחבורה":
        url = "How you like them apples?";
        break;
      case "תרבות ופנאי":
        url = "How you like them apples?";
        break;
      case "מיסים ותשלומים":
        url = "How you like them apples?";
        break;
      case "הלוואות ומשכנתא":
        url = "How you like them apples?";
        break;
      case "נופש":
        url = "How you like them apples?";
        break;
      case "שונות":
        url = "How you like them apples?";
        break;
      case "שיקים":
        url = "How you like them apples?";
        break;
      case "מזומן":
        url = "How you like them apples?";
        break;
      case "מתנות ומזכרות":
        url = "How you like them apples?";
        break;
      case "יופי ואיבזרי אופנה":
        url = "How you like them apples?";
        break;
      case "ביטוח":
        url = "How you like them apples?";
        break;
      case "ביטוח":
        url = "How you like them apples?";
        break;
      default:
        url = "I have never heard of that fruit...";
    }
    return url;
  }
}
