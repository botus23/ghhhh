import { Ingredient } from '../shared/ingredient.model';

export class Recipe {
  public name: string;
  public category: string;
  public imagePath: string;
  public ingredients: Ingredient[];
  public money: number;
  public date: Date;
  public type: string;

  constructor(name: string, catg: string, imagePath: string, ingredients: Ingredient[],money: number) {
    this.name = name;
    this.category = catg;
    this.imagePath = imagePath;
    this.ingredients = ingredients;
    this.money = money;
  }
}
