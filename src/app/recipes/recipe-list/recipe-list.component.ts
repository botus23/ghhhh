import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, Params, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';

import { Recipe } from '../recipe.model';
import { RecipeService } from '../recipe.service';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit, OnDestroy {
  recipes: Recipe[];
  income: Recipe[];
  currntrecipes: Recipe[];
  currntincome: Recipe[];
  subscription: Subscription;
  subscription2: Subscription;
  subscription3: Subscription;
  test: string;

  constructor(private recipeService: RecipeService,
    private router: Router,
    private route: ActivatedRoute) {
  }

  ngOnInit() {
    // this.route.params
    //   .subscribe(
    //     (params: Params) => {
    //       this.test =params['type'];
    //     })
    this.subscription3 = this.recipeService.TESTCHANGE
      .subscribe(
        (recipes: any) => {
          this.test = recipes;
          console.log("TESTCHANGE=" + (recipes))
        }
      );

    this.subscription = this.recipeService.recipesChanged
      .subscribe(
        (recipes: Recipe[]) => {
          console.log("RECIPCTCHANGED=" + JSON.stringify(recipes))
          if (recipes[0].type == "הוצאה") {
            this.recipes = recipes;
            this.recipes = this.recipeService.getRecipes();
          }
          else {
            this.income = recipes;
            this.income = this.recipeService.getIncomes();
          }
        }
      );

    this.subscription2 = this.recipeService.recipesChanged2
      .subscribe(
        (recipes: Recipe[]) => {  
          console.log("222RECIPCTCHANGED2" + JSON.stringify(recipes))
          if (recipes[0].type == "הוצאה") {
            this.currntrecipes = recipes;
            this.currntrecipes = this.recipeService.getRecipes2();
          }
          else {
            this.currntincome = recipes;
            this.currntincome = this.recipeService.getIncomes2();
          }
        }
      );
  }

  onNewRecipe(type2: string) {
    this.router.navigate(['new'], { queryParams: { type: type2 }, relativeTo: this.route });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
    this.subscription2.unsubscribe();
    this.subscription3.unsubscribe();
  }
}
