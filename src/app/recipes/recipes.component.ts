import { Component, OnInit,ChangeDetectionStrategy  } from '@angular/core';
import { RecipeService } from './recipe.service';


@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'app-recipes',
  templateUrl: './recipes.component.html',
  styleUrls: ['./recipes.component.css']
})
export class RecipesComponent implements OnInit {
  constructor(private recipeService: RecipeService) { }
  ngOnInit() {
  }

  activeSlideIndex = 6;
  slideChangeMessage = '';
  months = ["ינואר", "פברואר", "מרץ", "אפריל",
  "מאי", "יוני", "יולי", "אוגוסט", "ספטמבר",
  "אוקטובר", "נובמר", "דצמבר"];
 
  log(event: number) {
    this.slideChangeMessage = `Month has been switched: ${event+1}`;
    this.Check(event+1);
  }

  Check(month) {
    this.recipeService.thisMonth(month);
  }


}
