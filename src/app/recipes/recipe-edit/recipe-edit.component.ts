import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { FormGroup, FormControl, FormArray, Validators } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import { RecipeService } from '../recipe.service';
import { Recipe } from '../recipe.model';


@Component({
  selector: 'app-recipe-edit',
  templateUrl: './recipe-edit.component.html',
  styleUrls: ['./recipe-edit.component.css']
})
export class RecipeEditComponent implements OnInit {
  id: number;
  editMode = false;
  recipeForm: FormGroup;
  summi = 0;
  type = "";
  private subscription: Subscription;
  recipe: Recipe;

  constructor(private route: ActivatedRoute,
    private recipeService: RecipeService,
    private router: Router) {
  }

  ngOnInit() {
    this.route.params
      .subscribe(
        (params: Params) => {
          this.type = params['type'];
          this.id = +params['id'];
          console.log("IN EDIT: id " + this.id + " string is" + this.type);
          this.editMode = params['id'] != null;
          console.log("IN EDIT: editMode +" + this.editMode);
          // only if we click new it send with data type
          if (!this.editMode) {
            this.type = this.route.snapshot.queryParamMap.get('type');
            console.log("this.type URL " + this.type);
          }
          this.initForm();
        }
      );
    this.onChanges();
  }

  onChanges(): void {
    this.subscription = this.recipeForm.get('category').valueChanges.subscribe((val) => {
      console.log(val);
      this.recipeForm.get('imagePath').patchValue(this.recipeService.setPicCat(val))
    });
  }

  onSubmit() {
    if (this.editMode) {
      this.recipeService.updateRecipe(this.id, this.recipeForm.value);
    } else {
      console.log("BEFORE ADD" + JSON.stringify(this.recipeForm.value))
      this.recipeService.addRecipe(this.recipeForm.value);
    }
    this.onCancel();
  }
  onAddIngredient() {
    (<FormArray>this.recipeForm.get('ingredients')).push(
      new FormGroup({
        'name': new FormControl(null, Validators.required),
        'amount': new FormControl(null, [
          Validators.required,
          Validators.pattern(/^[1-9]+[0-9]*$/)
        ])
      })
    );
  }

  onDeleteIngredient(index: number) {
    (<FormArray>this.recipeForm.get('ingredients')).removeAt(index);
  }

  onCancel() {
    this.router.navigate(['../'], { relativeTo: this.route });
  }

  public initForm() {
    let recipeName = '';
    let recipeImagePath = '';
    let recipeCategory = '';
    let recipeMoney = 0;
    let recipeDate: Date = new Date();
    let recipeType = this.type;
    let recipeIngredients = new FormArray([]);
    let recipe2: Recipe;

    if (this.editMode) {
      if (this.type == "הוצאה") {
        recipe2 = this.recipeService.getRecipe(this.id);
        console.log("inEDIT.testהוצאה")
      }
      else {
        recipe2 = this.recipeService.getIncome(this.id);
        console.log("inEDIT.testהכנסה")
        console.log("inEDIT.testהכנסה"+recipe2);
      }
      recipeName = recipe2.name;
      recipeCategory = recipe2.category;
      recipeImagePath = recipe2.imagePath;
      recipeMoney = recipe2.money;
      recipeDate = recipe2.date;
      recipeType = this.type;
      if (recipe2['ingredients']) {
        for (let ingredient of recipe2.ingredients) {
          recipeIngredients.push(
            new FormGroup({
              'name': new FormControl(ingredient.name, Validators.required),
              'amount': new FormControl(ingredient.amount, [
                Validators.required,
                Validators.pattern(/^[1-9]+[0-9]*$/),
              ])
            })
          );
          this.summi += ingredient.amount;
        }
      }
    }
    this.recipeForm = new FormGroup({
      'name': new FormControl(recipeName, Validators.required),
      'imagePath': new FormControl(recipeImagePath),
      'category': new FormControl(recipeCategory, Validators.required),
      'money': new FormControl(recipeMoney, Validators.required),
      'date': new FormControl(recipeDate, Validators.required),
      'type': new FormControl(recipeType),
      //,this.MoneyCheck.bind(this)]
      'ingredients': recipeIngredients
    });
    console.log("FINISH INIZI"+this.recipeForm);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  MoneyCheck(control: FormControl): { [s: string]: boolean } {
    if (control.value != this.summi) {
      return { 'ERROR': true };
    }
    return null;
  }

  get money() {
    return this.recipeForm.get('money');
  }
  get ingredients() {
    return this.recipeForm.get('ingredients');
  }

}
