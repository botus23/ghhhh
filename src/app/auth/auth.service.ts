import { Router } from '@angular/router';
import * as firebase from 'firebase';
import { Injectable } from '@angular/core';
import { DataStorageService } from '../shared/data-storage.service';

@Injectable()
export class AuthService {
  token: string;
  uid: string;

  constructor(private router: Router,private dataStorageService: DataStorageService,) {}

  signupUser(email: string, password: string) {
    firebase.auth().createUserWithEmailAndPassword(email, password)
      .catch(
        error => console.log(error)
      )
  }

  signinUser(email: string, password: string) {
    firebase.auth().signInWithEmailAndPassword(email, password)
      .then(
        response => {
          this.router.navigate(['/']);
          firebase.auth().currentUser.getIdToken ()
            .then(
              (token: string) => this.token = token
            ),
            this.uid = firebase.auth().currentUser.uid.toString ();
            console.log("UID ="+this.uid);
            console.log("TOKEN ="+this.token);
            // this.dataStorageService.getRecipes();
            // this.dataStorageService.getIncomes();
        }
      )
      .catch(
        error => console.log(error)
      );
  }

  logout() {
    firebase.auth().signOut()
        .then(
          response => {
            this.router.navigate(['/']);
            this.token = null
            this.uid = null
          });
  }

  getToken() {
    firebase.auth().currentUser.getIdToken()
      .then(
        (token: string) => this.token = token
      );
    return this.token;
  }

  getUid() {
    return this.uid;
  }

  isAuthenticated() {
    return this.token != null;
  }
}
